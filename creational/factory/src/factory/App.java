package factory;

public class App {
    public static void main(String[] args) {
        var coin1 = CoinFactory.getCoin(CoinType.GOLD);
        var coin2 = CoinFactory.getCoin(CoinType.COPPER);

        System.out.println(coin1.getDescription());
        System.out.println(coin2.getDescription());
    }
}
