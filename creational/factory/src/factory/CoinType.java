package factory;

import java.util.function.Supplier;

public enum CoinType {
    GOLD(GoldCoin::new),
    COPPER(CopperCoin::new);

    CoinType(Supplier<Coin> constructor) {
        this.constructor = constructor;
    }

    private final Supplier<Coin> constructor;

    public Supplier<Coin> getConstructor() {
        return constructor;
    }
}
