package singleton;

public final class FinalClass {

    private static final FinalClass instance = new FinalClass();

    private FinalClass() {};

    public static FinalClass getInstance() {
        return instance;
    }
}
